#!/bin/bash
docker run --network isolated_nw --name express1-test --rm $1 \
        -v /home/ec2-user/express1-test:/mnt/stuff \
        -w /mnt/stuff node:alpine npm run firststart
