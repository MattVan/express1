# express1

Jobs to do to get test route working

We need in the deploy file to add with changes:

deploy-test.sh
    Line 20: change updateAndRestart to updateAndRestart-test
    
    Change DEPLOY_SERVER to TEST_SERVER and add that variable to gitlab with
    key=TEST_SERVER and value being app-test.494909.xyz

express1-test.sh
    Line 2: change express1 to express1-test
    
nginx-test-redirect.conf
    change app.494909.xyz to app-test.494909.xyz line 3 and 9
    
nginx-test-tls.conf
    Line 3 & 9 & 10 change app.494909.xyz to app-test.494909.xyz
    Line 16: change express1 to express1-test
    
nginx-test.conf:
    Change line 3 from app.494909.xyz to app-test.494909.com
    Line 10: change express1 to express1-test
    
updateAndRestart-test.conf
    Line 3, 6, 12, 19: change express to express1-test
    Add git checkout express1-test under cd /home/ec2-user/express1-test
    
Add, Commit, and Push all changes to gitlab    

We have to run our express1-test container

Copy to the ~etc/nginx/conf.d folder: 
    nginx-tls.conf
    nginx-redirect.conf
    nginx-redirect-test.conf
    nginx-test-tls.conf

Restart the containers
    
For the second part of the assignment, since we already have our certificate set up
we just have to add the nginx-redirect.conf to the ~/etc/nginx/conf.d folder
to ensure the reroute is handled properly.  We already took care of altering those files